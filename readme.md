# DSV - ES6

## What is ES6?
* [Compatibility
Table](http://kangax.github.io/compat-table/es6/)
* Tool for transpiling ES6:
    * Babel.js (most notable)
    * Traceur
    * Closure

## Transpiling ES6
* #### Introduction to ES6
    * ES6 code in -> ES5 code out <br/>
    * Used frequently with React <br/>
    * [Babel](https://babeljs.io)
* #### In-browser Babel transpiling
    * [cdnjs](https://cdnjs.com/libraries/babel-core "A compiler for writing next generation JS")
    * remember to change the script `type` of the ES6 file to `text/babel`
    * [WebPack](https://webpack.github.io)
    * ES6 is now supported more widely, and more importantly, babel has updated. So this part is somewhat unnecessary.

## ES6 Syntax
* `let` keyword
    we use `let` keyword to create block scoping (in a loop or a logic statement,..)
* `const` keyword
* **Template string**: ` `Hello ${variable}` `
* **Spread operators**: we can add an arr elements into an arr by using `...<array-name>`

## ES6 Functions & Objects
* #### Enhancing object literals
    * can replace `sleep: function(var){}` with `sleep(var){}`
* #### Arrow functions
    * shorten JS function, make it more compact (sure) and readable (..not so sure).
    * refer *Code.01* for example
    * it can also be used to resolve `this` scope problem without using `bind`
    * Reference [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions "MDN") and [here](https://kipalog.com/posts/ECMA-Script-6-fat-arrow-function "Vietnamese")
* #### Destructuring assignment:
    * use to assign name to array member/object member
    * or use to pass a object property into a function
    * refer to Code.02, Code.03 and Code.04 for example or read online reference [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment "MDN")
* #### Generators
    * **Generator** is a new type of function that allow us to pause a function at the middle of execution, to be resumed later. It's marked by an asterisk `*` follow the `function` keyword. (like this: `funtion* functionNam() {}`)
    * we hit pause within a function by using `yield` keyword, and continuing by using the `next` keyword
    * reference [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function* "MDN") and [here](https://kipalog.com/posts/function--va-yield-trong-Javascript-generator-function "Vietnamese")

## ES6 Classes
* #### ES6 Class Syntax:
    * to make a declaration of a new class, we must have a constructor (refer to Code.05)
    * to refer to a property in the class, you must use `this.propertyName`
* #### Class inheritance
    * `super()` function is going to refer to the parent class and can use its constructor



## Code Reference
#### Code.01
```javascript
var studentList = function(students){
    console.log(students);
}
//Arrow function ver:
var studentList = (students) => console.log(students);
//or even better with just 1 variable:
var studentList = students => console.log(students);
```
#### Code.02
```javascript
//So we have this:
var studentList = ["John", "Mary", "Thomas"];
var me = studentList[0]; //=John
//But we can also do this
var [s1,, s3,] = ["John", "Mary", "Thomas"];
var me = s1; //=John
var you = s3; //=Thomas
```
#### Code.03
```javascript
//so we have this:
var book = {
    title: "Cosmos",
    author: "Carl Sagan",
};
console.log(book.title);
//but this also is cool
var {title, author}{
    title: "Cosmos",
    author: "Carl Sagan",
}
console.log(title);
```
#### Code.04
```javascript
//it's an object
var book = {
    title: "Cosmos",
    author: "Carl Sagan",
};
//we can make a function like this:
function infoPrint({title, author}){
    return `This book is ${title} by ${author}`;
}
//And then here comes object
console.log(infoPrint(book));
//cool, right?
```
#### Code.05
```javascript
class Book{
    constructor(title, author){
        this.title = title;
        this.author = author;
    }
}
```
